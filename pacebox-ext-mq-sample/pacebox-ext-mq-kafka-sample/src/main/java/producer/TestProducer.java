package producer;

import org.junit.Test;
import tech.mhuang.pacebox.mq.admin.MqBuilder;
import tech.mhuang.pacebox.mq.admin.MqFramework;
import tech.mhuang.pacebox.mq.kafka.external.KafkaExternal;
import tech.mhuang.pacebox.mq.producer.bean.MqProducerBean;

public class TestProducer {

    @Test
    public void producer(){
        MqBuilder mqBuilder = new MqBuilder();
        MqProducerBean producerBean = mqBuilder.createProducerBuilder().enable(true).servers("192.168.1.210:9092").build();
        MqFramework mqFramework = mqBuilder.enableProducer(true).bindProducer("test",producerBean).builder();
        //采用kafka
        mqFramework.mqExternal(new KafkaExternal());
        mqFramework.start();
        mqFramework.getSuccessProducerMap().get("test").send("test","name","你好",(result,error)->{
            System.out.println(result);
            System.err.println(error);
        });
        System.out.println("====1=====");
    }
}
