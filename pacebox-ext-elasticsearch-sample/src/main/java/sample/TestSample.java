package sample;


import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.AcknowledgedResponse;
import co.elastic.clients.elasticsearch.core.DeleteResponse;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.UpdateResponse;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import tech.mhuang.pacebox.elasticsearch.admin.ESBuilder;
import tech.mhuang.pacebox.elasticsearch.admin.ESFramework;
import tech.mhuang.pacebox.elasticsearch.admin.factory.IESFactory;
import tech.mhuang.pacebox.elasticsearch.server.annoation.ESTable;

import java.io.IOException;

/**
 * 测试
 *
 * @author mhuang
 * @since 1.0.0
 */
@Slf4j
public class TestSample {

    public static void main(String[] args) {
        //获取ES构建类
        ESBuilder.Builder builder = ESBuilder.builder();
        //创建ES生产
        ESBuilder.ProducerBuilder producerBuilder = builder.createProducerBuilder();
        //绑定一个zhangsan地址210到framework,可同时绑定多个
        ESFramework framework = builder.bindProducer("zhangsan", producerBuilder.url("http://192.168.1.210:9200").enable(true).builder()).builder();
        //启动ES平台
        framework.start();
        //根据绑定的key获取对应的工厂
        IESFactory factory = framework.getFactory("zhangsan");
        try {
            //ES新增
            IndexResponse response = factory.insert("{\"age\":22}", "test");
            log.info("新增1:{}", response);
            Test test = new Test();
            IndexResponse response2 = factory.insert(test);
            log.info("新增2:{}", response2);
            String id = response2.id();
            test.setName("你好");
            //ES修改
            UpdateResponse response3 = factory.update(test, id);
            log.info("修改:{}", response3);
            //ES删除数据
            DeleteResponse response4 = factory.delete("test", id);
            log.info("删除:{}", response4);
            //ES删除索引及数据
            AcknowledgedResponse response5 = factory.delete("test");
            log.info("删除索引:{}", response5);
            //获取RestHighLevelClient对象
            ElasticsearchClient client = factory.getClient();
            log.info("打印信息{}", client.info());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Data
    @ESTable(index = "test")
    static class Test {
        private String name = "张三";
    }
}
