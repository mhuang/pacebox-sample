package sample.async;


import tech.mhuang.pacebox.core.async.AsyncTask;

/**
 * 成功的测试
 */
public class SuccessAsync implements AsyncTask<String> {

    @Override
    public void onSuccess(String result) {
        System.out.println(result);
    }

    @Override
    public void onFailed(Throwable t) {
        t.printStackTrace();
    }

    @Override
    public String execute() {
        return "我成功执行了方法";
    }
}
