package sample.async;


import tech.mhuang.pacebox.core.async.AsyncTask;

/**
 * 失败的测试
 */
public class FaildAsync implements AsyncTask<String> {

    @Override
    public void onSuccess(String result) {
        System.out.println(result);
    }

    @Override
    public void onFailed(Throwable t) {
        t.printStackTrace();
    }

    @Override
    public String execute() {
        System.out.println(1/0);
        return "我失败执行了方法";
    }
}
