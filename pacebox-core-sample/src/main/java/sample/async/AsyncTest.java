package sample.async;


import tech.mhuang.pacebox.core.async.AsyncTask;
import tech.mhuang.pacebox.core.async.AsyncTaskService;
import tech.mhuang.pacebox.core.async.DefaultAsyncTaskSupport;

/**
 * 测试-异步方式
 *
 * @author mhuang
 */
public class AsyncTest {

    public static void main(String[] args) {
        AsyncTaskService<String> task = new DefaultAsyncTaskSupport();
        AsyncTask<String> successTask = new SuccessAsync();
        AsyncTask<String> faildTask = new FaildAsync();
        task.submit(faildTask);
        task.submit(successTask);
    }
}
