package sample.clone;


import tech.mhuang.pacebox.core.clone.DefaultCloneableSupport;

/**
 *
 * 克隆测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestClone extends DefaultCloneableSupport<TestClone> {

    private String name;
    public static void main(String[] args) {
        TestClone test = new TestClone();
        test.name = "张三";
        TestClone clone = test.clone();
        System.out.println(clone.name == test.name);
        System.out.println(clone == test);
    }
}
