package sample.file;


import tech.mhuang.pacebox.core.file.FileUtil;
import tech.mhuang.pacebox.core.io.IOUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * 文件测试工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestFile {

    public static void main(String[] args) {
        try {
            //参照apache 中的FileUtil实现、提供常用的方法使用方式
            File file = new File("d://1text.txt");
            //创建
            //创建文件
            FileUtil.createFile(file);
            //创建文件夹
            FileUtil.createDirectory(new File("d:\\12"));
            //创建文件夹下级联创建
            FileUtil.createDirectory(new File("d:\\123\\12"), true);

            //写入文件
            //将123写入文件、覆盖之前文件里的数据
            FileUtil.write(file, "123");
            FileUtil.writeStringToFile(file, "123");
            FileUtil.writeByteArrayToFile(file, "123".getBytes());
            //追加字符串
            FileUtil.write(file, "你好", true);
            FileUtil.writeStringToFile(file, "你好", true);
            FileUtil.writeByteArrayToFile(file, "你好".getBytes(), true);

            //查看结果
            System.out.println(FileUtil.readFileToString(file));
            System.out.println(FileUtil.readFileToByteArray(file));

            //拷贝
            //将URL远程资源copy到本地 先创建需要拷贝到的文件、然后指定url进行拷贝
            FileUtil.createFile(new File("d:\\abcabc\\test.jpg"));
            FileUtil.createFile(new File("d:\\abcabc\\test-copy.jpg"));
            FileUtil.copyURLToFile(new URL("https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3300305952,1328708913&fm=27&gp=0.jpg"),
                    new File("d:\\abcabc\\test.jpg"));
            InputStream in = FileUtil.openInputStream(new File("d:\\abcabc\\test.jpg"));
            FileUtil.copyToFile(in,new File("d:\\abcabc\\test-copy.jpg"));
            IOUtil.close(in);
            //移动
            FileUtil.moveFile(new File("d:\\abcabc\\test-copy.jpg"),
                    new File("d:\\abcabc\\123.jpg"));
            FileUtil.moveDirectory(new File("d:\\abcabc"),new File("d:\\abcabc1"));
            //删除
            //删除文件或文件夹
            FileUtil.delete(file);
            //删除文件或文件夹、删除失败会抛出异常
            FileUtil.forceDelete(new File("d:\\123\\12"));
            //删除文件夹、删除失败会抛出异常
            FileUtil.deleteDirectory(new File("d:\\12"));
            //清空文件夹下的所有文件
            FileUtil.cleanDirectory(new File("d:\\abcabc1"));

            FileUtil.deleteDirectory(new File("d:\\abcabc1"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
