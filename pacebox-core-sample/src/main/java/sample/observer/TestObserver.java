package sample.observer;


import tech.mhuang.pacebox.core.observer.AbstractObServer;
import tech.mhuang.pacebox.core.observer.AbstractSubject;
import tech.mhuang.pacebox.core.observer.BaseObServer;
import tech.mhuang.pacebox.core.observer.BaseSubject;
import tech.mhuang.pacebox.core.observer.ObFactory;
import tech.mhuang.pacebox.core.observer.ObserverType;

import java.util.concurrent.ExecutionException;

/**
 * 测试观察者
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestObserver {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        //将订阅者添加到工厂
        ObFactory.add("simple", new SimpleSubject());
        //获取工厂中得主题
        BaseSubject simpleSubject = ObFactory.get("simple");
        //设置主题的观察者、设置三个、测试1是同步、其余是异步
        SimpleObServer simple1 = new SimpleObServer();
        simple1.type(ObserverType.SYNC).name("测试1");
        SimpleObServer simple2 = new SimpleObServer();
        simple2.type(ObserverType.NSYNC).name("测试2");
        SimpleObServer simple3 = new SimpleObServer();
        simple3.type(ObserverType.NSYNC).name("测试3");
        //设置主题的自定义观察者
        ExtObServer extObServer = new ExtObServer();
        extObServer.name = "你好";
        extObServer.type = ObserverType.SYNC;
        //给主题添加观察者
        simpleSubject.add(simple1);
        simpleSubject.add(simple2);
        simpleSubject.add(simple3);
        //给主题添加自定义扩展的消费者
        simpleSubject.add(extObServer);
        //主题方式消息
        simpleSubject.send("你好");
    }

    /**
     * 案例主题
     */
    static class SimpleSubject extends AbstractSubject<String> {

        @Override
        public void send(String data) throws ExecutionException, InterruptedException {
            //给所有得观察者发送消息
            for (BaseObServer<String> observer : this.getObServerList()) {
                observer.execute(data);
            }
        }
    }

    /**
     * 案例观察者
     */
    static class SimpleObServer extends AbstractObServer<String> {

        @Override
        protected void execute() {
            System.out.println(
                    "观察者执行了任务" +
                            "名称"+this.getName()+
                            ",状态"+this.getType()+
                            ",全部执行" + this.getExecuteCount() + "次" +
                            ",执行时间:" + this.getLastReqTime() +
                            ",数据是:" + this.data
            );
        }
    }

    /**
     * 自定义主题
     */
    static class ExtObServer implements BaseObServer{

        private String name;
        private ObserverType type;

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public ObserverType getType() {
            return this.type;
        }

        @Override
        public void execute(Object data) {
            System.out.println(
                    "扩展观察者执行了任务" +
                            "名称"+this.getName()+
                            ",状态"+this.getType()+
                            ",数据是:" + data
            );
        }
    }
}
