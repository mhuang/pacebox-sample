package sample;


import tech.mhuang.pacebox.core.builder.BaseBuilder;

/**
 * @package: tech.mhuang.hmtool.core.sample
 * @author: mhuang
 * @Date: 2019/7/12 15:24
 * @Description:
 */

public class Teacher {
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public static class TeacherBuilder implements BaseBuilder<Teacher> {

        private final Teacher teacher = new Teacher();

        public TeacherBuilder name(String name){
            this.teacher.setName(name);
            return this;
        }

        public TeacherBuilder age(Integer age){
            this.teacher.setAge(age);
            return this;
        }

        @Override
        public Teacher builder() {
            return this.teacher;
        }
    }

}
