package sample.reflect;


import tech.mhuang.pacebox.core.reflect.BaseReflectInvoke;
import tech.mhuang.pacebox.core.reflect.DefaultReflectInvoke;

import java.lang.reflect.InvocationTargetException;

/**
 *
 * 测试反射调用
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestReflect {

    public String getK(String name){
        return "hello"+name;
    }
    public String getHello(){
        return "hello";
    }
    public static void main(String[] args) {
        BaseReflectInvoke invoke = new DefaultReflectInvoke();
        try {
            /**
             * 测试调用TestReflect类的getK方法传递name参数后获取到的值
             */
            Object o = invoke.getMethodToValue(TestReflect.class,"getK","张三");
            System.out.println(o);
            /**
             * 测试调用TestReflect类的getHello方法获取到的值
             */
            Object o1 = invoke.getMethodToValue(TestReflect.class,"getHello");
            System.out.println(o1);
        } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
