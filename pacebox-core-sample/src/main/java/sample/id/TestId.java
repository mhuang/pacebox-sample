package sample.id;


import tech.mhuang.pacebox.core.id.BaseIdeable;
import tech.mhuang.pacebox.core.id.DefaultIdeable;
import tech.mhuang.pacebox.core.id.SnowflakeIdeable;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * 生成id测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestId {

    public static void main(String[] args) {
        List<String> list = new CopyOnWriteArrayList<>();
        //雪花算法随机数
        BaseIdeable<String> snowflake = new SnowflakeIdeable();
        Runnable runnable = ()->{
            int i = 0;
            while(i++ < 10000){
                String genratorId = snowflake.generateId();
                if(list.contains(genratorId)){
                    System.out.println(genratorId+"重复雪花算法");
                }else{
                    list.add(genratorId);
                }
            }
        };
        new Thread(runnable).start();
        new Thread(runnable).start();
        new Thread(runnable).start();

        BaseIdeable<String> uuid = new DefaultIdeable();
        Runnable runnable1 = () -> {
            int i = 0;
            while(i++ < 10000){
                String genratorId = uuid.generateId();
                if(list.contains(genratorId)){
                    System.out.println(genratorId+"重复UUID");
                }else{
                    list.add(genratorId);
                }
            }
        };

        new Thread(runnable1).start();
        new Thread(runnable1).start();
        new Thread(runnable1).start();
    }
}
