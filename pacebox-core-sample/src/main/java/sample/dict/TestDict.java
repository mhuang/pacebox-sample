package sample.dict;


import sample.Teacher;
import tech.mhuang.pacebox.core.dict.BasicDict;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 字典类测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestDict {

    public static void main(String[] args) {
        Teacher.TeacherBuilder build =  new Teacher.TeacherBuilder();
        Teacher teacher = build.age(20).name("李四").builder();
        List<String> l = new ArrayList<>();
        l.add("22");
        BasicDict basicDict = new BasicDict();
        basicDict.set("name","张三").set("age",18).set("teacher",teacher).set("list",l);
        System.out.println("打印字典数据"+basicDict);
        System.out.println("打印字典中某个健的实体数据"+basicDict.get("teacher",Teacher.class));
        System.out.println("打印字典中某个健的列表数据"+basicDict.get("list",List.class));
    }
}
