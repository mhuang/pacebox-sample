package sample.random;


import tech.mhuang.pacebox.core.random.RandomUtil;

/**
 *
 * 随机数测试类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestRandom {

    public static void main(String[] args) {
        int i = 0;
        while(++i<10){
            //随机生成1000-9999的随机数
            System.out.println(RandomUtil.getInRange(1000,9999));
            //随机生成int随机数（有可能为负数）
            System.out.println(RandomUtil.nextInt());
        }
    }
}
