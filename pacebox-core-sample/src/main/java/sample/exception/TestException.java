package sample.exception;


import sample.Teacher;
import tech.mhuang.pacebox.core.exception.ExceptionUtil;

/**
 *
 * 异常测试类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestException {

    public static void main(String[] args) {
        try{
            Teacher teacher = null;
            teacher.setAge(11);
        }catch(Exception e){
            System.out.println(ExceptionUtil.getMessage(e));
        }
    }
}
