package sample.pool;


import tech.mhuang.pacebox.core.pool.BaseExecutor;
import tech.mhuang.pacebox.core.pool.DefaultThreadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * 线程池测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestPool {

    public static void main(String[] args) {
        //默认使用
        BaseExecutor pool = new DefaultThreadPool();
        pool.submit(() -> {
            String name = "测试1";
            int count = 0;
            while (count ++ <100){
                System.out.println(name + "你好" + count);
            }

        });
        pool.submit(() -> {
            String name = "测试2";
            int count = 0;
            while (count ++ <100){
                System.out.println(name + "你好" + count);
            }
        });
        //自定义线程池使用
        ExtPool extPool = new ExtPool();
        extPool.submit(()->{
            String name = "自定义测试";
            int count = 0;
            while (count ++ <100){
                System.out.println(name + "你好" + count);
            }
        });
    }

    static class ExtPool implements BaseExecutor{

        private ExecutorService executorService = null;

        public ExtPool(){
            executorService = Executors.newCachedThreadPool();

        }


        @Override
        public Future<?> submit(Runnable command) {
            return executorService.submit(command);
        }

        @Override
        public void execute(Runnable command) {
            executorService.execute(command);
        }
    }
}
