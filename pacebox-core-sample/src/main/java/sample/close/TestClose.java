package sample.close;


import tech.mhuang.pacebox.core.close.BaseCloseable;

/**
 * 测试关闭
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestClose implements BaseCloseable {
    @Override
    public void close() {
        System.out.println("关闭了");
        //TODO 此处可用于对象回收、比如引用设置为空等等
    }

    public static void main(String[] args) {
        TestClose test = new TestClose();
        test.close();
    }
}
