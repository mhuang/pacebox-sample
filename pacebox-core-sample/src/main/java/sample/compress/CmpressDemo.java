//package sample.compress;
//
//import org.junit.Test;
//
//import java.io.IOException;
//
///**
// * @author yuanhang.huang
// * @version 1.0.0
// * @date 2020/11/25 17:04
// */
//
//public class CmpressDemo {
//
//    private static String ROOT = System.getProperty("user.dir");
//
//    private static BaseCompress compress = CompressTemplate.getInstance();
//
//    private static String source_path = ROOT + "/src/main/resources/compress/test";
//    private static String zip_dest_file = ROOT + "/src/main/resources/compress/ziptest.zip";
//    private static String zip_source_file = ROOT + "/src/main/resources/compress/ziptest.zip";
//    private static String zip_dest_dir = ROOT + "/src/main/resources/compress/ziptest";
//
//    private static String win_rar_exe = "C:\\Program Files\\WinRAR\\";
//    private static String rar_dest_file = ROOT + "/src/main/resources/compress/rartest.rar";
//    private static String rar_dest_dir = ROOT + "/src/main/resources/compress/rartest";
//
//    @Test
//    public void zipTest() throws IOException, InterruptedException {
//
//        System.out.println("===========================================");
//
//
//        System.out.println(source_path);
//
//        compress.compress(source_path, zip_dest_file, true, ZipCompressHandler.CODE);
//
//        System.out.println("===========================================");
//
//    }
//
//    @Test
//    public void unzipTest() throws IOException, InterruptedException {
//
//        System.out.println("===========================================");
//
//        compress.decompress(zip_source_file, zip_dest_dir, true, true, ZipCompressHandler.CODE);
//
//        System.out.println("===========================================");
//
//    }
//
//    @Test
//    public void rarTest() throws IOException, InterruptedException {
//
//        System.out.println("===========================================");
//
//        CmdCompressHandler handler = (CmdCompressHandler) compress.getHandler(RarCmdCompressHandler.CODE);
//        handler.setCmdDir(win_rar_exe);
//        handler.compress(source_path, rar_dest_file, true);
//
//        System.out.println("===========================================");
//
//    }
//
//    @Test
//    public void unrarTest() throws IOException, InterruptedException {
//
//        System.out.println("===========================================");
//
//        CmdCompressHandler handler = (CmdCompressHandler) compress.getHandler(RarCmdCompressHandler.CODE);
//        handler.setCmdDir(win_rar_exe);
//        handler.decompress(rar_dest_file, rar_dest_dir, true, true);
//
//        System.out.println("===========================================");
//
//    }
//
//}
