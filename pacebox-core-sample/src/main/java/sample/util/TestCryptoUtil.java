package sample.util;


import tech.mhuang.pacebox.core.util.CryptoUtil;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 *
 * 测试加密工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestCryptoUtil {

    public static void main(String[] args) {
        try {
            //md5
            System.out.println(CryptoUtil.encrypt("zhangsan","md5"));
            //sha-1
            System.out.println(CryptoUtil.encrypt("zhangsan","sha-1"));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
