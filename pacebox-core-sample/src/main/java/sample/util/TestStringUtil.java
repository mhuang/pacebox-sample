package sample.util;



import tech.mhuang.pacebox.core.util.StringUtil;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * 字符串工具类测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestStringUtil {

    public static void main(String[] args) {
        //判断是否是空
        System.out.println(StringUtil.isEmpty(""));
        System.out.println(StringUtil.isNotEmpty(""));
        //判断是否是空并且若是NULL字符串也代表是空
        System.out.println(StringUtil.isEmptyNull("NULL"));
        System.out.println(StringUtil.isNotEmptyNull("null"));
        //判断是否是空、过滤空格
        System.out.println(StringUtil.isNotBlank(""));
        System.out.println(StringUtil.isBlank(""));
        //判断2个字符串是否相等
        System.out.println(StringUtil.equals("1","1"));
        //将集合转string、逗号隔开
        System.out.println(StringUtil.join(Stream.of("21","22").collect(Collectors.toList()), ","));
        //判断字符串大小、默认是小于比较的字符串、若为false则是大于比较的字符串
        System.out.println(StringUtil.compare("a","b"));
        System.out.println(StringUtil.compare(null, "a",false));
        //忽略大小写判断是否包含对应的字符串
        System.out.println(StringUtil.containsIgnoreCase("ABC","ab"));
        //长度
        System.out.println(StringUtil.length("11"));
        //截取字符串成数组
        System.out.println(Objects.requireNonNull(StringUtil.split("saassa1,sasasa2", ","))[1]);
        //去掉字符串中的空格
        System.out.println(StringUtil.trim("sasaas sasasaa"));
        //截取出现第一个e分离后的字符串
        System.out.println(StringUtil.substringAfter("aceded","e"));
        //截取最后出现的e分离前的字符串
        System.out.println(StringUtil.substringBeforeLast("aceded","e"));
        //获取字符串中e中第一次出现的下标
        System.out.println(StringUtil.indexOf("aceded","e"));
    }
}