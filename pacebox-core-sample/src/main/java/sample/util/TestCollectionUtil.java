package sample.util;


import tech.mhuang.pacebox.core.util.CollectionUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * 集合工具类测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestCollectionUtil {

    public static void main(String[] args) {
        /**
         * 判断集合对象是否为空、包含list、map、数组
         */
        System.out.println(CollectionUtil.isEmpty(new ArrayList<>()));
        System.out.println(CollectionUtil.isNotEmpty(new ArrayList<>()));
        System.out.println(CollectionUtil.isEmpty(new HashMap<>()));
        System.out.println(CollectionUtil.isNotEmpty(new HashMap<>()));
        System.out.println(CollectionUtil.isEmpty(new Object[]{}));
        System.out.println(CollectionUtil.isNotEmpty(new Object[]{}));
    }
}
