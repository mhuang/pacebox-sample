package sample.util;


import tech.mhuang.pacebox.core.util.PathUtil;

/**
 * 路径
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestPathUtil {

    public static void main(String[] args) {
        //获取根据class以及jar包名称获取路径
        System.out.println(PathUtil.getJarPash(PathUtil.class, "pacebox-core-1.0.5"));
    }
}
