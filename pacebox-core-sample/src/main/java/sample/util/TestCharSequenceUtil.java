package sample.util;


import tech.mhuang.pacebox.core.util.CharSequenceUtil;

/**
 *
 * 字符序列工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestCharSequenceUtil {

    public static void main(String[] args) {
        /**
         * 返回字符序列开始后的字符序列，
         */
        System.out.println(CharSequenceUtil.subSequence("211221",1));
    }
}
