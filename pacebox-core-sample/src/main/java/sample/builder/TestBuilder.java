package sample.builder;


import tech.mhuang.pacebox.core.builder.BaseBuilder;

/**
 * @author mhuang
 * @since 1.0.0
 */
public class TestBuilder implements BaseBuilder<String> {

    private final String name = "张三";

    @Override
    public String builder() {
        return name;
    }

    public static void main(String[] args) {
        BaseBuilder<String> builder = new TestBuilder();
        String name = builder.builder();
        System.out.println(name);
    }
}
