package sample.convert;


import tech.mhuang.pacebox.core.convert.AbstractConverter;
import tech.mhuang.pacebox.core.convert.Converter;
import tech.mhuang.pacebox.core.convert.ConverterFactory;

/**
 *
 * 测试转换器
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestConvert {

    public static void main(String[] args) {
        System.out.println(Converter.convert(Integer.class, 1,0));
        System.out.println(Converter.convert(String.class, "1" ,"0"));
        System.out.println(Converter.convert(Boolean.class, true ,false));
        AbstractConverter<Integer> intConverter = new AbstractConverter<>() {
            @Override
            public Integer convert(Object source) throws IllegalArgumentException {
                if (source instanceof String str) {
                    return Integer.valueOf(str);
                }
                return (Integer) source;
            }
        };
        ConverterFactory.getInstance().addConverter(Integer.class, intConverter);
        System.out.println(Converter.convert(Integer.class, "1" ,0));
    }

}
