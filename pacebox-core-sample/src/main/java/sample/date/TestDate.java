package sample.date;


import tech.mhuang.pacebox.core.date.DatePattern;
import tech.mhuang.pacebox.core.date.DateTimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 *
 * 日期测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestDate {

    public static void main(String[] args) {
        Date currentDate = DateTimeUtil.currentDate();
        LocalDateTime localDateTime = DateTimeUtil.dateToLocalDateTime(currentDate);
        LocalDate localDate = DateTimeUtil.dateToLocalDate(currentDate);
        //unit
        System.out.println("unit时间戳（毫秒）转LocalDateTime"+DateTimeUtil.unitToDateTime(System.currentTimeMillis()));

        ///date操作相关
        System.out.println("当前时间（Date类型）："+DateTimeUtil.currentDate());
        System.out.println("Date转LocalDateTime:"+DateTimeUtil.dateToLocalDateTime(currentDate));
        System.out.println("Date转LocalDate:"+DateTimeUtil.dateToLocalDate(currentDate));

        //JDK8 LocalDate 操作相关
        System.out.println("LocalDate转换成Data、时间取00:00:00 "+DateTimeUtil.localDateToEndDate(localDate));
        System.out.println("LocalDate转换成Data、时间取23:59:59 "+DateTimeUtil.localDateToEndDate(localDate));
        System.out.println("获取某日期当月的天数"+DateTimeUtil.getMonthDayNumByLocalDate(LocalDate.now()));
        System.out.println("返回开始日期到结束日期的相差天数"+DateTimeUtil.getLocalDateDiffDay(LocalDate.now(),LocalDate.now().plusDays(20)));
        System.out.println("获取指定日期一周的数据"+DateTimeUtil.localDateToThisWeekList(LocalDate.now()));
        System.out.println("判断是不是日期是不是本周"+DateTimeUtil.localDateIsThisWeek(LocalDate.now()));
        System.out.println("获取指定日期一月的数据"+DateTimeUtil.localDateToThisMonthList(LocalDate.now()));
        //JDK8 LocalDateTime操作相关
        System.out.println("LocalDateTime格式化日期、默认标准格式"+DateTimeUtil.fromDateTime(localDateTime));
        System.out.println("LocalDateTime格式化日期、指定格式字符串"+DateTimeUtil.fromDateTime(localDateTime,
                DatePattern.PURE_DATETIME_PATTERN));
        System.out.println("LocalDateTime格式化日期、指定格式化类"+DateTimeUtil.fromDateTime(localDateTime,
                DatePattern.CHINESE_DATE_FORMAT));
        System.out.println("LocalDateTime转换成日期格式"+DateTimeUtil.localDateTimeToDate(localDateTime));
        System.out.println("LocalDateTime转unit时间戳（毫秒级）"+DateTimeUtil.getTimestampOfDateTime(localDateTime));

    }
}
