package sample.editor;


import sample.Teacher;
import tech.mhuang.pacebox.core.editor.BaseEditor;
import tech.mhuang.pacebox.core.util.ObjectUtil;
import tech.mhuang.pacebox.core.util.StringUtil;

/**
 *
 * 编辑器测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestEditor implements BaseEditor<Teacher> {

    private Teacher teacher;
    @Override
    public Teacher edit(Teacher teacher) {
        if(this.teacher == null){
            this.teacher = teacher;
            return this.teacher;
        }
        if(ObjectUtil.isNotEmpty(teacher.getAge())){
            this.teacher.setAge(teacher.getAge());
        }
        if(StringUtil.isNotEmpty(teacher.getName())){
            this.teacher.setName(teacher.getName());
        }
        return this.teacher;
    }

    public static void main(String[] args) {
        Teacher.TeacherBuilder build = new Teacher.TeacherBuilder();
        Teacher teacher = build.name("张三").builder();
        TestEditor editor = new TestEditor();
        System.out.println(editor.edit(teacher));
        teacher.setName(null);
        teacher.setAge(19);
        System.out.println(editor.edit(teacher));
    }
}
