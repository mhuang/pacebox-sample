package sample.check;


import tech.mhuang.pacebox.core.check.CheckAssert;

/**
 *
 * 测试检查
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestCheck {

    public static void main(String[] args) {
        CheckAssert.check(null);
        CheckAssert.check(null,"空值异常");
        CheckAssert.check(null,new RuntimeException("空值异常"));
        CheckAssert.check(false, "传递参数异常");
    }
}
