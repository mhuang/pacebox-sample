package sample;


import sample.client.TestNettyClient;
import tech.mhuang.pacebox.netty.client.MyNettyClient;

/**
 * @package: com.hmtool.test
 * @author: mhuang
 * @Date: 2019/6/14 17:08
 * @Description:
 */
public class TestClient {

    public static void main(String[] args) {
        MyNettyClient nettyClient = new TestNettyClient();
        nettyClient.connect("127.0.0.1", 8181);
    }
}
