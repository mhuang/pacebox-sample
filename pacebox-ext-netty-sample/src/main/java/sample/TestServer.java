package sample;

import sample.server.TestNettyServer;
import tech.mhuang.pacebox.netty.server.MyNettyServer;


/**
 *
 * 测试netty server服务
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestServer {
    public static void main(String[] args) {
        MyNettyServer myNettyServer = new TestNettyServer();
        myNettyServer.bind(8181);
    }
}