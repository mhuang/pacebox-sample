package sample.handler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import sample.server.TestNettyServerHandler;
import tech.mhuang.pacebox.netty.coder.ServerDecoder;
import tech.mhuang.pacebox.netty.coder.ServerEncoder;


/**
 * @ClassName: ServerChannelInitializerHandler
 * @Description:服务端初始化过滤器
 * @author: mhuang
 * @date: 2017年12月19日 下午5:20:06
 */
public class TestServerChannelInitializerHandler extends ChannelInitializer<SocketChannel> {

    private static final ServerEncoder encoder = new ServerEncoder();

    public TestServerChannelInitializerHandler() {
        super();
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(encoder);
        pipeline.addLast(new ServerDecoder());
        pipeline.addLast(new TestNettyServerHandler());
    }
}
