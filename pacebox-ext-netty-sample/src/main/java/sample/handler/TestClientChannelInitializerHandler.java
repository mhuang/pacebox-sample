package sample.handler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import sample.client.TestNettyClientHandler;
import tech.mhuang.pacebox.netty.coder.ClientDecoder;
import tech.mhuang.pacebox.netty.coder.ClientEncoder;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName: ClientChannelInitializerHandler
 * @Description:客户端初始化拦截器
 * @author: mhuang
 * @date: 2017年12月19日 下午5:19:37
 */
public class TestClientChannelInitializerHandler extends ChannelInitializer<SocketChannel> {

    private static final ClientEncoder encoder = new ClientEncoder();

    public TestClientChannelInitializerHandler() {
        super();
    }

    @Override
    protected void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new IdleStateHandler(30, 10, 0, TimeUnit.SECONDS));
        pipeline.addLast(encoder);
        pipeline.addLast(new ClientDecoder());
        pipeline.addLast(new TestNettyClientHandler());
    }
}
