package sample.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sample.handler.TestClientChannelInitializerHandler;
import tech.mhuang.pacebox.core.close.BaseCloseable;
import tech.mhuang.pacebox.netty.client.AbstractNettyClient;



/**
 * @author mHuang
 * @version V1.0.0
 * @Description netty 客户端 Demo
 * @date 2015年7月16日 下午2:55:36
 */
public class TestNettyClient extends AbstractNettyClient implements BaseCloseable {

    private static final long serialVersionUID = 1L;

    public static final Channel channel = null;

    @Override
    public void connect(String host, int port) {
        //config thread pool
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            //config client aotu property
            Bootstrap b = new Bootstrap();
            b.group(group).channel(NioSocketChannel.class).handler(new TestClientChannelInitializerHandler());
            // Start the client.
            ChannelFuture f = b.connect(host, port).sync();
            // Wait until the connection is closed.
            f.channel().closeFuture().sync();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }

    @Override
    public void connect() {
        connect(host, port);
    }


    @Override
    public void close() {
        TestNettyClient.channel.close();
    }

    static class ClientTask implements Runnable {
        /**
         * 日志对象
         */
        protected final Logger logger = LoggerFactory.getLogger(ClientTask.class);

        @Override
        public void run() {
            try {
                while (true) {
                    Thread.sleep(10000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
