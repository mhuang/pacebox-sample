package sample.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.mhuang.pacebox.netty.vo.BaseMessage;

import java.io.Serializable;

/**
 * @author huang.miao
 * @Package: tech.mhuang.vo
 * @Description 心跳
 * @date 2017年1月16日 上午11:36:02
 * @group skiper-opensource
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TestP0002 extends BaseMessage implements Serializable {

    public static final int P0002 = 0x0002;//心跳

    private static final long serialVersionUID = 1L;

    public TestP0002() {
        getHeader().setMsgId(P0002);
    }
}
