package sample;


import tech.mhuang.pacebox.kafka.producer.bean.KafkaMsg;

/**
 *
 * 消费者测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestConsumer {

    public void test(KafkaMsg msg){
        System.out.println(msg);
    }
}
