package sample;


import tech.mhuang.pacebox.kafka.admin.KafkaBuilder;
import tech.mhuang.pacebox.kafka.admin.KafkaFramework;

/**
 * kafka消费者测试
 *
 * @author mhuang
 * @since 1.0.0
 */
public class TestCreateConsumer {

    public static void main(String[] args) {
        //获取kafka组装器
        KafkaBuilder.Builder builder = KafkaBuilder.builder();
        //开启消费者
        builder.enableConsumer(true);
        //获得消费者对应构造器
        KafkaBuilder.ConsumerBuilder consumerBuilder = builder.createConsumerBuilder();
        consumerBuilder.enable(true).servers("192.168.1.210:9092").topics("zhangsan")
                .invokeBeanName("tech.mhuang.ext.kafka.sample.TestConsumer")
                .invokeMethodName("test");
        builder.bindConsumer("consumer", consumerBuilder.builder());
        KafkaFramework framework = builder.builder();
        framework.start();
    }
}
